package main

import "github.com/gin-gonic/gin"

var ninjaWeapons = map[string]string{
	"ninjaStar":"Beginner ninja star"
}

func GetWeapon(c *gin.Context) {
	weaponType := c.Query("type")
	weaponName, ok := ninjaWeapons[weaponType]
	if !ok {
		c.JSON(404, gin.H{
			weaponType: "",
		})
		return
	}
	c.JSON(200, gin.H{
		weaponType : weaponName
	})
}

func main() {
	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
	r.GET("/weapon", GetWeapon)
	r.POST("/weapon", GetWeapon)
	r.DELETE("/weapon", GetWeapon)
	r.Run()
}
